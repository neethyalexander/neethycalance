package basic.concepts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class CalanceDimensionTask {

	public static void main(String[] args) throws InterruptedException
    {
		// TODO Auto-generated method stub
		WebDriver driver=new FirefoxDriver();

//to open https://dev-dimension.calance.us		
        driver.get(" https://dev-dimension.calance.us  ");

//to maximize the window
        driver.manage().window().maximize();
//to login
        driver.findElement(By.xpath("html/body/div/div[1]/div[2]/a[1]")).click();
        driver.findElement(By.xpath(".//*[@id='show_dimension']")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath(".//*[@id='inputs']/input[1]")).sendKeys("nalexander");
        driver.findElement(By.xpath(".//*[@id='inputs']/input[2]")).sendKeys("neethy");
        driver.findElement(By.xpath(".//*[@id='company_id']")).sendKeys("MOCK");
        driver.findElement(By.xpath(".//*[@id='submit']")).click();
        Thread.sleep(1000);
//searching for the process with Animal[sheep] and task[find] which executed with a warning message
        WebElement mytable  = driver.findElement(By.id("mytable")) ;
        driver.findElement(By.xpath(".//*[@id='mytable']/thead/tr/th[2]/input")).sendKeys(" Animal:[sheep] Task:[find]");
        driver.findElement(By.xpath(".//*[@id='mytable']/thead/tr/th[4]/input")).sendKeys("Warning");
        Thread.sleep(1000);
//to re execute the task
        driver.findElement(By.xpath(".//*[@id='mytable']/tbody/tr[1]/td[6]/form/button")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath(".//*[@id='mytable']/thead/tr/th[3]/input")).sendKeys("Neethy Alexander");

//for finding details of the produced bug
        String time=driver.findElement(By.xpath(".//*[@id='mytable']/tbody/tr[1]/td[1]")).getText();
        System.out.println(time);
        String task=driver.findElement(By.xpath(".//*[@id='mytable']/tbody/tr[1]/td[2]")).getText();
        System.out.println(task);
        String message=driver.findElement(By.xpath(".//*[@id='mytable']/tbody/tr[1]/td[4]")).getText();
        System.out.println(message);

//Verifying if the task failed with Warning message
        if(task.contains(" Animal:[sheep] Task:[find]")&& message.contains("Warning"))
        {
	        System.out.println("Successful reproduction of bug");
        }
}
